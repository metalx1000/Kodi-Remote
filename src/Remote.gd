extends Control

var ir_ip="192.168.1.12"
var kodi_ip="192.168.1.146"

onready var status = $VBoxContainer/status
onready var power = $VBoxContainer/HBoxContainer/POWER
onready var ir_btns = get_tree().get_nodes_in_group("ir_btn")
onready var kodi_btns = get_tree().get_nodes_in_group("kodi_btn")
# Called when the node enters the scene tree for the first time.
func _ready():

	for btn in ir_btns:
		btn.connect("pressed",self,"ir_send",[btn.code])
		
	for btn in kodi_btns:
		btn.connect("pressed",self,"kodi_send",[btn.cmd])
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func ir_send(code):
	var url = "http://" + ir_ip + "/ir?code=" + code
	status.text = url + "  -  "
	var http_request = HTTPRequest.new()
	add_child(http_request)
	http_request.connect("request_completed", self, "_on_request_completed", [http_request])
	http_request.request(url)
	
func kodi_send(cmd):
	var url = "http://" + kodi_ip + ":8080/cgi-bin/" + cmd + ".cgi"
	status.text = url + "  -  "
	var http_request = HTTPRequest.new()
	add_child(http_request)
	http_request.connect("request_completed", self, "_on_request_completed", [http_request])
	http_request.request(url)
	#http_request.queue_free()

func _on_request_completed(result, response_code, headers, body,node):
	status.text += str(response_code)
	print(headers)
	print(body.get_string_from_utf8())
	node.queue_free()

